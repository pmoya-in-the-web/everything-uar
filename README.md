# Everything UAR ( Up And Running )

This project is intended to ease installation & setup of some software I use in my home computer. 

All scripts should work properly in [opensuse](https://www.opensuse.org/).


The result of running a specific installation script should be that you get the software "up and running" ( UAR ) .

## Software packages

* KeepassXC
* Boinc
* digikam
* backintime
* Akregator
* some tools - iotop, ntop, fish ("friendly interactive shell"), git

* kodi
* Qbittorrent
* Resilio Sync
* Signal
* Telegram
* Firefox (just if not Tumbleweed)
* Chromium
* 

## Repositroy structure
each software package / program in an specific directory

## Instructions

### Run all scripts and install all software

### Running an specific script
