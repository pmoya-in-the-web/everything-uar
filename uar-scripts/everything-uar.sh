#!/bin/sh

echo '*** Validating requirements ***'
# Checks if the user running the script is root
if [ $EUID != 0 ];
  then
    echo -e "\e[1;31mPlease run as root (try using 'su' or 'sudo' )'\e[0m"
    exit 1
  else
    echo 'Script is runned by "root" user'
fi


echo "Installing"

sh ./utils-uar/utils-uar.sh
# sh ./boinc-uar/boinc-uar.sh

echo "Done"