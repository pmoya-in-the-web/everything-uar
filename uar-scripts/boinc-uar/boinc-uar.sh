#!/bin/sh

# BOINC UAR script

echo "Installing boinc"

zypper --non-interactive in boinc-client boinc-manager

# get the user from system variables or somewhere
usermod -G boinc -a $USER

systemctl start boinc-client

# ToDo: Configure AppArmor profile

echo "BOINC service (boinc-client) will be running on background and started automatically when your system starts"
echo "The user " $USER " has been configured in order to be able to execute boinc-manager, if you want other users to to it type; usermod -G boinc -a userName"
echo "First time you launch Boinc Manager go to \"Archive / Select computer\" and connect to 127.0.0.1, password is under /var/lib/boinc/gui_rpc_auth.cfg"
echo "Remember to add projects and configure computing preferences"