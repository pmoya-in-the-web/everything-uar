#!/bin/sh

# Utils UAR script

utilsToInstall=( git iotop fish solaar ) 

echo "Installing utils; " ${utilsToInstall[@]}

zypper --non-interactive in ${utilsToInstall[@]} 

echo "Utils installed"
